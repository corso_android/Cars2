package com.example.cars;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements ICarHandler {

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container,
                false);
        ButterKnife.bind(this, rootView);
        ICarHandler handler = this;
        AsyncTaskCar taskCar = new AsyncTaskCar(getContext(), handler);
        taskCar.execute();
        return rootView;
    }

    @Override
    public void onSuccess(List<Car> cars) {
        for (Car car: cars) {
            Log.d("DEBUG", car.getPlate()+" "+car.getModel());
        }
    }

    @Override
    public void onError(String message) {

    }
}
