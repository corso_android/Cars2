package com.example.cars;


import java.util.List;

public interface ICarHandler {

    void onSuccess(List<Car> cars);

    void onError(String message);
}
