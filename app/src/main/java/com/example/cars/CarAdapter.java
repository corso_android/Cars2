package com.example.cars;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;


public class CarAdapter extends BaseAdapter {

    Context context;
    List<Car> cars;
    LayoutInflater inflter;

    public CarAdapter(Context context, List<Car> cars) {
        this.context = context;
        this.cars = cars;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return cars.size();
    }

    @Override
    public Object getItem(int i) {
        return cars.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        Car car = cars.get(position);
        if(view==null) {
            view = inflter.inflate(R.layout.layout_list_view_car, null);
            viewHolder = new ViewHolder(view, car);

            viewHolder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            view.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.car = car;
        viewHolder.tvPlate.setText(viewHolder.car.getPlate());
        viewHolder.tvInfo.setText(viewHolder.car.getModel()+" "+viewHolder.car.getSeats());
        return view;
    }

    public class ViewHolder {
        LinearLayout row;
        ImageView imageView;
        TextView tvPlate;
        TextView tvInfo;
        Car car;

        public ViewHolder(View view, Car car){
            this.row = (LinearLayout) view.findViewById(R.id.row);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);
            this.tvPlate = (TextView) view.findViewById(R.id.tvPlate);
            this.tvInfo = (TextView) view.findViewById(R.id.tvInfo);
            this.car = car;
        }
    }
}

