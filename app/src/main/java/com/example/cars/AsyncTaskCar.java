package com.example.cars;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

public class AsyncTaskCar extends AsyncTask<Void, String, List<Car>> {


    private ProgressDialog progressDialog;
    private Context context;
    private ICarHandler handler;


    public AsyncTaskCar(Context context, ICarHandler handler) {
        this.context = context;
        this.handler = handler;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = ProgressDialog.show(context,
                "Login",
                "Caricamento...");
    }


    @Override
    protected List<Car> doInBackground(Void... params) {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("plate","model1",4, null));
        cars.add(new Car("plate2","model2",5, null));
        cars.add(new Car("plate3","model4",3, null));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return cars;
    }


    @Override
    protected void onPostExecute(List<Car> cars) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        if(cars!=null){
            handler.onSuccess(cars);
        }else{
            handler.onError("Errore");
        }
    }



}
