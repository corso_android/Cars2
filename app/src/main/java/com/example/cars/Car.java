package com.example.cars;


import android.graphics.Bitmap;

public class Car {

    private Integer pk;
    private String plate;
    private String model;
    private Integer seats;
    private Bitmap image;

    public Car(Integer pk, String plate, String model, Integer seats, Bitmap image) {
        this.pk = pk;
        this.plate = plate;
        this.model = model;
        this.seats = seats;
        this.image = image;
    }

    public Car(String plate, String model, Integer seats, Bitmap image) {
        this.plate = plate;
        this.model = model;
        this.seats = seats;
        this.image = image;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
